# Global variables
PYTHON_INTERPRETER = python3

export PATH := $(shell pwd)/:$(PATH)

.PHONY: clean-build
clean-build: ## Remove build artifacts
	@echo "+ $@"
	@rm -fr build/
	@rm -fr dist/
	@rm -fr *.egg-info

.PHONY: clean-pyc
clean-pyc: ## Remove Python file artifacts
	@echo "+ $@"
	@find . -type d -name '__pycache__' -exec rm -rf {} +
	@find . -type f -name '*.py[co]' -exec rm -f {} +
	@find . -name '*~' -exec rm -f {} +

.PHONY: docs
docs: ## Rebuild docs automatically and display locally.
	mkdocs build --config-file mkdocs.yml --strict --verbose

.PHONY: servedocs
servedocs: ## Rebuild docs automatically and push to github.
	mkdocs build --config-file docs/mkdocs.yml --strict --verbose
	git add --all
	git commit -m "Updates to Website"
	git push origin main
	@echo "Website updated! Check it out: https://thiagoasilva.gitlab.io "