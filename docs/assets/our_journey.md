# Our Buyer Profile

## Who We Are
Thiago and Lindsey Silva are actively seeking to acquire and operate a business. With Thiago's background in IT and operations management, combined with Lindsey's expertise in sales and relationship building, we bring a complementary skill set to business ownership.

## Business Criteria
### What We're Looking For
- **Revenue**: $[X]M - $[Y]M
- **EBITDA**: $[X]K - $[Y]K
- **Industries**: 
    - B2B Services
    - Technology Companies
    - Service-Based Businesses
    - [Other specific industries]
- **Location**: [Your preferred areas]
- **Characteristics**:
    - Established customer base
    - Recurring revenue model
    - Strong team in place
    - Growth potential

## Our Advantages as Buyers

### Financial Position
- Committed capital from personal savings
- Pre-arranged financing
- Professional advisory team in place
- Ready for quick, clean transaction

### Professional Background
#### Thiago
- 15+ years in IT and Operations
- Expert in process optimization
- Strategic planning background
- Financial management experience

#### Lindsey
- 10+ years in Sales
- Customer relationship expert
- Team building specialist
- Operations management

## Transaction Approach
1. **Quick Response**: 24-48 hour initial response
2. **Professional Process**: Working with [Advisor Name] for transaction support
3. **Clear Timeline**: 60-90 day target close
4. **Smooth Transition**: Structured handover plan

## Contact Information
- **Email**: [Your email]
- **Phone**: [Your phone]
- **LinkedIn**: [Your LinkedIn]

[Schedule a Call](#){: .md-button .md-button--primary}