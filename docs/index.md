# Thiago & Lindsey Silva
## Business Buyers Ready to Move Forward

![Thiago_handsome](images/thiagosilva.jpg)

### At a Glance
- **Investment Range**: $[X] - $[Y]
- **Location**: [Your target geographical area]
- **Timeline**: Ready to move forward immediately
- **Industries**: B2B Services, Technology, or Service-Based Businesses
- **EBITDA**: $[X] - $[Y]

[Contact Us Now](#contact){: .md-button .md-button--primary}

---

## Why Work With Us
- **Fast Decision Making**: Committed buyers with financing ready
- **Tech & Sales Expertise**: Combined 20+ years experience
- **Strong Team**: IT/Operations (Thiago) + Sales/Relations (Lindsey)
- **Clean Exit**: Structured approach to business transition